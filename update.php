//Srun Srorn "update.php"
<?php
// Include connect database
require('connection.php');

if(isset($_POST['edit'])){
    $edit_id = $_GET['id'];
    $edit_phone_num = $_POST['edit_phone_num'];
    $edit_fname = $_POST['edit_fname'];
    $edit_lname = $_POST['edit_lname'];
    $edit_url_fb = $_POST['edit_url_fb'];
    $edit_profile = $_FILES['edit_profile'];
        
    // File name
    $pro_name = rand(100,1000000) ."-". $edit_profile["name"];

    // Temporary name
    $tmpName = $edit_profile['tmp_name'];

    // Directory to upload file
    $destination = "upload_profile_img/".$pro_name; 

    // Move file
    $profile_picture = move_uploaded_file($tmpName, $destination);
    $sql_update =  "UPDATE phone_contact SET phone_number = '$edit_phone_num',
                    firstname = '$edit_fname', lastname = '$edit_lname', url_fb_social = '$edit_url_fb',
                    picture_profile= '$pro_name' WHERE id = $edit_id";
    $update_query = mysqli_query($conn, $sql_update);
    
    if ($update_query) {
        echo "<script>alert('Update Successful!')</script>";
        header("Location: index.php");
    }else{
        echo "<script>alert('Update not Successful!')</script>";
        header("Location: index.php");
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD OPERATION</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
     rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
      crossorigin="anonymous">
</head>
<body>
    <?php
        if(isset($_GET['id'])) {
            $edit_id = $_GET['id'];
            $sql_select = mysqli_query($conn,"SELECT * FROM `phone_contact` WHERE id=$edit_id");
            $row = mysqli_fetch_assoc($sql_select); 
            //var_dump($row);
            $phone_num = $row['phone_number'];
            $fname = $row['firstname'];
            $lname = $row['lastname'];
            $url_fb = $row['url_fb_social'];
            $pic_pro = $row['picture_profile'];
        }
    ?>
    <div class="container d-flex justify-content-center align-items-center vh-100">
        <form action="" method="post" class="shadow p-4 rounded" enctype="multipart/form-data">
            <h2 class="text-center text-primary mb-3">Please, Fill the information</h2>
            <input type="hidden" name="id" class="form-control mb-3" value="<?php echo $edit_id ?>">
            <label for="phone_num">Phone Number:</label>
            <input type="text" name="edit_phone_num" class="form-control mb-3" value="<?php echo $phone_num ?>">
            <label for="fnam">First Name:</label>
            <input type="text" name="edit_fname" class="form-control mb-3" value="<?php echo $fname ?>">
            <label for="lname">Last Name:</label>
            <input type="text" name="edit_lname" class="form-control mb-3" value="<?php echo $lname ?>">
            <label for="url_fb">URL_FB_Social:</label>
            <input type="text" name="edit_url_fb" class="form-control mb-3" value="<?php echo $url_fb ?>">
            <label for="profile">Picture Profile:</label>
            <input type="file" name="edit_profile" class="form-control mb-3">
            <?php if ($pic_pro !== ''): ?>
                <img src="upload_profile_img/<?php echo $pic_pro ?>" alt="Profile Picture" class="mb-3" width="200">
            <?php endif; ?>
            <input type="submit" name="edit" value="Update" class="btn text-light btn-warning form-control">
        </form>
    </div>
</body>
</html>