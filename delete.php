//Mom Monychot "delete.php"
<?php
    // Include database connection
    require('connection.php');

    if (isset($_GET['delete'])) {
        $delete_id = $_GET['id'];
        
        // SQL delete from the database
        $delete_sql = "DELETE FROM phone_contact WHERE id = $delete_id";
        
        // Execute the SQL query
        if(mysqli_query($conn, $delete_sql)){
            echo "<script>alert('Contact deleted successfully!')</script>";
            header("Location: index.php");
            exit(); // Terminate the script after the redirect
        } else{
            echo "<script>alert('Can not delete contact!')</script>";
            header("Location: index.php");
            exit(); // Terminate the script after the redirect
        }
    }
?>