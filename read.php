//Leang Honghav "read.php"
<?php
include "connection.php";
?>

<!DOCTYPE html>
<html>

<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td,
        th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        td img{
            margin-left: 30px;
        }
        tr:nth-child(even) {
            background-color: #dddddd;
        }

        .container {
            width: 70%;
            margin: 5rem auto;
        }

        .head {
            background-color: gray;
            color: white;
        }

        button {
            padding: .5rem 1rem;
            cursor: pointer;
            border: none;
            color: white;
            border-radius: .2rem;
        }

        .edit {
            background-color: blue;
        }

        .delete {
            background-color: red;
        }

        .add {
            background-color: green;
            margin-bottom: 1rem;
        }

        a {
            color: white;
            text-decoration: none;
        }
    </style>
</head>

<body>
    <div class="container">
        <h2>Php CRUD</h2>
        <button class="add"><a href="insert.php">Add New</a></button>
        <table>
            <tr>
                <th class="head">ID</th>
                <th class="head">Phone Number</th>
                <th class="head">Firstname</th>
                <th class="head">Lastname</th>
                <th class="head">URl_FB_Social</th>
                <th class="head">Picture Profile</th>
                <th class="head">Action</th>
            </tr>
            <?php
            $sql = "SELECT * FROM phone_contact";
            $result = mysqli_query($conn, $sql);
            while ($row = mysqli_fetch_assoc($result)) {
                echo "
                        <tr>
                            <td>{$row['id']}</td>
                            <td>{$row['phone_number']}</td>
                            <td>{$row['firstname']}</td>
                            <td>{$row['lastname']}</td>
                            <td>{$row['url_fb_social']}</td>
                            <td>
                                <img width='50' src='upload_profile_img/{$row['picture_profile']}'>
                           </td>
                            <td>
                                <button class='edit'><a href='update.php?id={$row['id']}'>Edit</a></button>
                                <button class='delete'><a href='delete.php?delete&id={$row['id']}'>Delete</a></button>
                            </td>
                        </tr>
                    ";
            }
            ?>
        </table>
    </div>

</body>

</html>