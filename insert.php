//Sang Haksou "insert.php"
<?php
    // Include connect database
    require('connection.php');

    if(isset($_POST['submit'])){
        $id = $_POST['id'];
        $phone_num = $_POST['phone_num'];
        $fname = $_POST['fname'];
        $lname = $_POST['lname'];
        $url_fb = $_POST['url_fb'];
        $profile = $_FILES['pic_pro'];
        
        // File name
        $pro_name = rand(100,1000000) ."-". $profile["name"];

        // Temporary name
        $tmpName = $profile['tmp_name'];

        // Directory to upload file
        $destination = "upload_profile_img/".$pro_name; 

        // Move file
        $profile_picture = move_uploaded_file($tmpName, $destination);

        // SQL insert into the database
        $insert_sql = "INSERT INTO phone_contact (id, phone_number, firstname,
                       lastname, url_fb_social, picture_profile) VALUES 
                       ('$id', '$phone_num', '$fname', '$lname', '$url_fb', '$pro_name')";

        // Execute the SQL query
        if(mysqli_query($conn, $insert_sql)){
           echo "<script>alert('Add contact successfully!')</script>";
           header("Location: index.php");
        } else{
            echo "<script>alert('Can not Add contact!')</script>";
            header("Location: index.php");
            
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD OPERATION</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
     rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
      crossorigin="anonymous">
</head>
<body>
    <div class="container d-flex justify-content-center align-items-center vh-100">
        <form action="" method="post" class="shadow p-4 rounded" enctype="multipart/form-data">
            <h2 class="text-center text-primary mb-3">Please, Fill the information</h2>
            <label for="id">ID:</label>
            <input type="text" name="id" class="form-control mb-3">
            <label for="phone_num">Phone Number: </label>
            <input type="text" name="phone_num" class="form-control mb-3">
            <label for="fnam">First Name:</label>
            <input type="text" name="fname" class="form-control mb-3">
            <label for="lname">Last Name: </label>
            <input type="text" name="lname" class="form-control mb-3">
            <label for="url_fb">URL_FB_Social: </label>
            <input type="text" name="url_fb" class="form-control mb-3">
            <label for="profile">Picture Profile: </label>
            <input type="file" name="pic_pro" class="form-control mb-3">
            <input type="submit" name="submit" value="Submit" class="btn btn-success form-control">
        </form>
    </div>
</body>
</html>